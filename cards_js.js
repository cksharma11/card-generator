const cards = [
    {
      "type": "club",
      "number": "1",
      "color": "black",
      "unicode": "&#127185"
    },
    {
      "type": "club",
      "number": "2",
      "color": "black",
      "unicode": "&#127186"
    },
    {
      "type": "club",
      "number": "3",
      "color": "black",
      "unicode": "&#127187"
    },
    {
      "type": "club",
      "number": "4",
      "color": "black",
      "unicode": "&#127188"
    },
    {
      "type": "club",
      "number": "5",
      "color": "black",
      "unicode": "&#127189"
    },
    {
      "type": "club",
      "number": "6",
      "color": "black",
      "unicode": "&#127190"
    },
    {
      "type": "club",
      "number": "7",
      "color": "black",
      "unicode": "&#127191"
    },
    {
      "type": "club",
      "number": "8",
      "color": "black",
      "unicode": "&#127192"
    },
    {
      "type": "club",
      "number": "9",
      "color": "black",
      "unicode": "&#127193"
    },
    {
      "type": "club",
      "number": "10",
      "color": "black",
      "unicode": "&#127194"
    },
    {
      "type": "club",
      "number": "jack",
      "color": "black",
      "unicode": "&#127195"
    },
    {
      "type": "club",
      "number": "queen",
      "color": "black",
      "unicode": "&#127197"
    },
    {
      "type": "club",
      "number": "king",
      "color": "black",
      "unicode": "&#127198"
    },
    {
      "type": "spade",
      "number": "1",
      "color": "black",
      "unicode": "&#127137"
    },
    {
      "type": "spade",
      "number": "2",
      "color": "black",
      "unicode": "&#127138"
    },
    {
      "type": "spade",
      "number": "3",
      "color": "black",
      "unicode": "&#127139"
    },
    {
      "type": "spade",
      "number": "4",
      "color": "black",
      "unicode": "&#127140"
    },
    {
      "type": "spade",
      "number": "5",
      "color": "black",
      "unicode": "&#127141"
    },
    {
      "type": "spade",
      "number": "6",
      "color": "black",
      "unicode": "&#127142"
    },
    {
      "type": "spade",
      "number": "7",
      "color": "black",
      "unicode": "&#127143"
    },
    {
      "type": "spade",
      "number": "8",
      "color": "black",
      "unicode": "&#127144"
    },
    {
      "type": "spade",
      "number": "9",
      "color": "black",
      "unicode": "&#127145"
    },
    {
      "type": "spade",
      "number": "10",
      "color": "black",
      "unicode": "&#127146"
    },
    {
      "type": "spade",
      "number": "jack",
      "color": "black",
      "unicode": "&#127147"
    },
    {
      "type": "spade",
      "number": "queen",
      "color": "black",
      "unicode": "&#127149"
    },
    {
      "type": "spade",
      "number": "king",
      "color": "black",
      "unicode": "&#127150"
    },
    {
      "type": "diamond",
      "number": "1",
      "color": "red",
      "unicode": "&#127169"
    },
    {
      "type": "diamond",
      "number": "2",
      "color": "red",
      "unicode": "&#127170"
    },
    {
      "type": "diamond",
      "number": "3",
      "color": "red",
      "unicode": "&#127171"
    },
    {
      "type": "diamond",
      "number": "4",
      "color": "red",
      "unicode": "&#127172"
    },
    {
      "type": "diamond",
      "number": "5",
      "color": "red",
      "unicode": "&#127173"
    },
    {
      "type": "diamond",
      "number": "6",
      "color": "red",
      "unicode": "&#127174"
    },
    {
      "type": "diamond",
      "number": "7",
      "color": "red",
      "unicode": "&#127175"
    },
    {
      "type": "diamond",
      "number": "8",
      "color": "red",
      "unicode": "&#127176"
    },
    {
      "type": "diamond",
      "number": "9",
      "color": "red",
      "unicode": "&#127177"
    },
    {
      "type": "diamond",
      "number": "10",
      "color": "red",
      "unicode": "&#127178"
    },
    {
      "type": "diamond",
      "number": "jack",
      "color": "red",
      "unicode": "&#127179"
    },
    {
      "type": "diamond",
      "number": "queen",
      "color": "red",
      "unicode": "&#127181"
    },
    {
      "type": "diamond",
      "number": "king",
      "color": "red",
      "unicode": "&#127182"
    },
    {
      "type": "heart",
      "number": "1",
      "color": "red",
      "unicode": "&#127153"
    },
    {
      "type": "heart",
      "number": "2",
      "color": "red",
      "unicode": "&#127154"
    },
    {
      "type": "heart",
      "number": "3",
      "color": "red",
      "unicode": "&#127155"
    },
    {
      "type": "heart",
      "number": "4",
      "color": "red",
      "unicode": "&#127156"
    },
    {
      "type": "heart",
      "number": "5",
      "color": "red",
      "unicode": "&#127157"
    },
    {
      "type": "heart",
      "number": "6",
      "color": "red",
      "unicode": "&#127158"
    },
    {
      "type": "heart",
      "number": "7",
      "color": "red",
      "unicode": "&#127159"
    },
    {
      "type": "heart",
      "number": "8",
      "color": "red",
      "unicode": "&#127160"
    },
    {
      "type": "heart",
      "number": "9",
      "color": "red",
      "unicode": "&#127161"
    },
    {
      "type": "heart",
      "number": "10",
      "color": "red",
      "unicode": "&#127162"
    },
    {
      "type": "heart",
      "number": "jack",
      "color": "red",
      "unicode": "&#127163"
    },
    {
      "type": "heart",
      "number": "queen",
      "color": "red",
      "unicode": "&#127165"
    },
    {
      "type": "heart",
      "number": "king",
      "color": "red",
      "unicode": "&#127166"
    }
  ]

  export default cards;