const fs = require("fs");

const cardTypes = ["club", "spade", "diamond", "heart"];
const characters = ["jack","queen","king"];
const counters = [127185,127137, 127169,127153];

const cardJSONGenrator = function() {
  while (cardTypes.length > 0) {
    let color = "red";
    if (cardTypes.length > 2) {
      color = "black";
    }
    for (let i = 1; i < 11; i++) {
      const cardData = `{
            "type":"${cardTypes[0]}",
            "number":"${i}",
            "color":"${color}",
            "unicode" : "&#${counters[0]++}"
        },`;
      fs.appendFileSync("./cards.json", cardData);
    }
    
    for(let i=0; i<3;i++){
        if(i == 1){
            counters[0]++;
        }
        const cardData = `{
            "type":"${cardTypes[0]}",
            "number":"${characters[i]}",
            "color":"${color}",
            "unicode": "&#${counters[0]++}"
        },`;
      fs.appendFileSync("./cards.json", cardData);
    }

    counters.shift();
    cardTypes.shift();
  }
};

cardJSONGenrator();
